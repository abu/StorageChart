
# StorageChart

Visualize the evolution of the storage need for my [Friendica](https://social.alfredbuehler.ch/profile/abu) instance.
The main script is run daily by cron, it collects

- the size used by MySQL database
- the size used by the storage directory

The received data over the recent 90 days is stored in a CSV file. The webpage shows both, a line chart and the relevant values.

![StorageChart](Screenshots/20240226084249.png)

## Build

- _npm install_ to get all dependencies.
- _npm run start_ for the local preview.
- _npm run build_ for the final version.
- _npm run lint_ for the linting the javascript code.
- _npm run format_ for beautify the javascript code.

## Authors & Contributors
- [Alfred Bühler](https://codeberg.org/abu) - Author

## License
- This project is distributed under the [MIT License](https://choosealicense.com/licenses/mit/ "Read The MIT license")
