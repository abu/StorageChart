/* eslint no-undef: 'off' */
export const data = chartdata

const step = 20
const yMin = step * Math.floor(Math.min(...data.datasets[0].data) / step)
const yMax = step * Math.ceil(Math.max(...data.datasets[0].data) / step)

function getScale(side) {
  return {
    position: side,
    min: yMin,
    max: yMax
  }
}

export const options = {
  responsive: true,
  maintainAspectRatio: false,
  scales: {
    A: getScale('left'),
    B: getScale('right')
  }
}
