#!/bin/bash
. $(dirname $0)/storage.env
TODAY=$(date "+%F")

calcDBSize () {
	SQLFILE=/tmp/storage.sql
	echo "SELECT table_schema, CEIL(SUM(data_length + index_length) / 1048576)
	FROM information_schema.TABLES WHERE table_schema = '$1';" > $SQLFILE
	DBSIZE=$(mysql -h 127.0.0.1 < $SQLFILE | grep "$1" | cut -f2)
	rm $SQLFILE
}

calcDBSize $DBNAME
STORAGE=$(du -sm ${STORAGEDIR} | cut -f1)

# Create CSV record
echo ${TODAY},${DBSIZE},${STORAGE} >> ${TARGET}

# Create JavaScript data file for WebChart
cd sub/app/storage
php -f csv2js.php ${TODAY} > friendica.js
cd
